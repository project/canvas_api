<?php

namespace Drupal\canvas_api;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\key\KeyRepositoryInterface;

/**
 * Canvas API Service class.
 */
class CanvasApiService {

  use StringTranslationTrait;
  const PER_PAGE = 100;

  /**
   * The API token from the key repository.
   *
   * @var string
   */
  private $token;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $config;

  /**
   * The HTTP method for the API call.
   *
   * @var string
   */
  private $method;

  /**
   * The client factory.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  private $clientFactory;

  /**
   * The endpoint for the API call.
   *
   * @var string
   */
  private $path;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private $logger;

  /**
   * The query parameters for the API call.
   *
   * @var array
   */
  private $params = [];

  /**
   * The LMS settings.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  private $lms;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ClientFactory $clientFactory,
    ConfigFactoryInterface $config,
    KeyRepositoryInterface $keyRepository,
    LoggerChannelFactoryInterface $loggerChannelFactory,
  ) {
    $this->logger = $loggerChannelFactory->get('canvas_api');
    $this->lms = $config->get('canvas_lms.settings');
    if ($key = $config->get('canvas_api.settings')->get('key')) {
      $this->token = $keyRepository->getKey($key)->getKeyValue();
    }
    else {
      $this->logger->error($this->t('No Canvas API key found'));
    }
    $this->clientFactory = $clientFactory;
  }

  /**
   * Sets the API endpoint URI.
   */
  public function setPath($path): CanvasApiService {
    $this->path = $path;
    return $this;
  }

  /**
   * Sets the HTTP method for the API call.
   */
  public function setMethod($method): CanvasApiService {
    $this->method = $method;
    return $this;
  }

  /**
   * Sets the query parameters for the API call.
   */
  public function setParams($params): CanvasApiService {
    $this->params = $params;
    return $this;
  }

  /**
   * Formats the base URL for the API call.
   */
  public function getUrlString(): string {
    $url = 'https://' . $this->lms->get('institution');
    switch ($this->lms->get('environment')) {
      case 'production':
        break;

      case 'test':
        $url .= '.test';
        break;

      case 'beta':
        $url .= '.beta';
        break;

      default:
        $this->logger->error($this->t('No Canvas environment set'));
    }
    return $url . '.instructure.com/api/v1/';
  }

  /**
   * Sets HTTP client options for the API call.
   */
  public function getClientOptions(): array {
    return [
      'headers' => [
        'Authorization' => 'Bearer ' . $this->token,
      ],
      'base_uri' => $this->getUrlString(),
    ];
  }

  /**
   * Execute the API request and return the response.
   *
   * @return array
   *   JSON-decoded response from CanvasApi.
   */
  public function request(): array {

    $client = $this->clientFactory->fromOptions($this->getClientOptions());
    $response = $client->request($this->method, $this->path, $this->getOptions());

    // Canvas limits the number of responses returned in a GET request. If we
    // pass per_page=100, we can get up to 100, but that's all; if there are
    // more, we need to get the "next" page, which is passed in the Link:
    // attribute in the response header.
    if ($this->method == 'GET') {
      $data = Json::decode($response->getBody());
      $next = $this->getNextUrl($response);
      // We use a counter as a failsafe to this becoming an infinite loop.
      $i = 0;
      while ($next) {
        $response = $client->get($next);
        $data = array_merge($data, Json::decode($response->getBody()));
        $next = $this->getNextUrl($response);
        $i++;
        if ($i > 50) {
          // THIS IS JUST A FAILSAFE.
          die('Over 50 iterations');
        }
      }
      return $data;
    }

    return Json::decode($response->getBody());
  }

  /**
   * Extracts the "next" url from the response header.
   *
   * @param object $response
   *   Http response.
   *
   * @return string
   *   Link to next URL if found, FALSE if not.
   */
  public function getNextUrl($response) {
    $linkHeader = $response->getHeader('Link');
    $link = reset($linkHeader);
    $matches = [];
    $pattern = '/,<(.+)>; rel="next"/';
    preg_match($pattern, $link, $matches);
    return $matches[1] ?? FALSE;
  }

  /**
   * Sets additional query options for the API call.
   */
  public function getOptions() {
    $options['query'] = 'per_page=' . self::PER_PAGE;
    if ($this->method == 'GET') {
      $options['query'] .= '&' . $this->buildQuery($this->params);
    }
    else {
      $options['form_params'] = $this->params;
    }
    return $options;
  }

  /**
   * Helper function to build the query.
   *
   * Array values passed to Canvas must be in the form foo[]=bar&foo[]=baz, but
   * http_build_query (used by the Guzzle Client) will encode this as
   * foo[0]=bar&foo[1]=baz, which will fail. This helper function will also
   * allow us to pass in parameters as arrays, i.e.:
   *    $canvas_api->params = array(
   *      'course' => array(
   *         'account_id' => 1,
   *         'name' => 'Course Name',
   *         'term_id' => 6,
   *      ),
   *    );
   *
   * @param array $params
   *   Query parameters to format.
   *
   * @return string
   *   Formatted query string.
   */
  public function buildQuery(array $params): string {

    $query = [];
    foreach ($params as $key => $param) {
      if (is_array($param)) {
        foreach ($param as $key2 => $value) {
          $index = is_int($key2) ? '' : $key2;
          $query[] = $key . '[' . $index . ']=' . $value;
        }
      }
      else {
        $query[] = $key . '=' . $param;
      }
    }
    return implode('&', $query);
  }

}
