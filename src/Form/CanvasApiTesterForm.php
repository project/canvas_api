<?php

namespace Drupal\canvas_api\Form;

use Drupal\canvas_api\CanvasApiService;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form to configure the settings for the CanvasApi integration.
 */
class CanvasApiTesterForm extends FormBase {

  const HTTP_METHODS = ['GET', 'POST', 'PUT', 'DELETE'];
  const WRAPPER = 'canvas-api-results';

  /**
   * The Canvas API service.
   *
   * @var \Drupal\canvas_api\CanvasApiService
   */
  protected $canvasApi;

  /**
   * Constructs a new CanvasApiTesterForm object.
   *
   * @param \Drupal\canvas_api\CanvasApiService $canvas_api
   *   The Canvas API service.
   */
  public function __construct(CanvasApiService $canvas_api) {
    $this->canvasApi = $canvas_api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('canvas_api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'canvas_api_tester';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $env = $this->config('canvas_lms.settings')->get('environment');
    $notice = $this->t('You will be querying against the <strong>@env</strong> environment', ['@env' => $env]);

    $form['notice'] = [
      '#markup' => $notice,
    ];

    $form['method'] = [
      '#type' => 'select',
      '#options' => self::HTTP_METHODS,
      '#title' => $this->t('HTTP Method:'),
    ];

    $form['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path'),
      '#required' => TRUE,
      '#description' => $this->t('The API path, <em>not including</em> /api/v1/'),
    ];

    $form['parameters'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Parameters'),
      '#description' => $this->t('A JSON representation of the parameters'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Test'),
      '#ajax' => [
        'callback' => '::fetchResultsCallback',
        'wrapper' => self::WRAPPER,
      ],
    ];

    $form['output'] = [
      '#prefix' => '<div id="' . self::WRAPPER . '">',
      '#suffix' => '</div>',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($parameters = $form_state->getValue('parameters')) {
      $decoded = JSON::decode($parameters);
      if (!$decoded) {
        $error = $this->t('JSON appears to be invalid');
        $form_state->setError($form, $error);
      }
      else {
        $form_state->set('decoded', $decoded);
      }
    }
  }

  /**
   * Results callback.
   */
  public function fetchResultsCallback(array &$form, FormStateInterface $form_state) {

    $response = [
      '#prefix' => '<div id="' . self::WRAPPER . '"><pre>',
      '#suffix' => '</pre></div>',
    ];

    if (!$form_state->getErrors()) {
      $method = $this->getMethod($form_state);
      $path = $form_state->getValue('path');
      $parameters = $form_state->get('decoded');

      $results = $this->canvasApi
        ->setMethod($method)
        ->setPath($path)
        ->setParams($parameters)
        ->request();
      $response['#markup'] = print_r($results, TRUE);
    }

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  private function getMethod($form_state) {
    $index = $form_state->getValue('method');
    return self::HTTP_METHODS[$index];
  }

}
