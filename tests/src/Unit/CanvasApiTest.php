<?php

namespace Drupal\Tests\canvas_api\Unit;

use Drupal\canvas_api\CanvasApiService;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\key\Entity\Key;
use Drupal\key\KeyRepository;
use Drupal\Tests\UnitTestCase;

/**
 * @group testing_example
 */
class CanvasApiTest extends UnitTestCase {

  private $api;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $key = $this->createStub(Key::class);
    $key->method('getKeyValue')
      ->willReturn('');
    $keyRepository = $this->createStub(KeyRepository::class);
    $keyRepository->method('getKey')
      ->willReturn($key);
    $clientFactory = $this->createStub(ClientFactory::class);
    $loggerChannelFactory = $this->createStub(LoggerChannelFactory::class);

    $configFactory = $this->getConfigFactoryStub([
      'canvas_lms.settings' => [
        'institution' => 'test_school',
        'environment' => 'test'
      ],
      'canvas_api.settings' => [
        'key' => 'test_key'
      ]
    ]);

    $this->api = new CanvasApiService($clientFactory, $configFactory,$keyRepository, $loggerChannelFactory);
  }
  /**
   * Data provider for testBuildQuery().
   */
  public function buildQueryProvider() {
    return [
      [
        [],
        ''
      ],
      [
        [
          'start_time' => '2020-12-31'
        ],
        'start_time=2020-12-31'
      ],
      [
        [
          'course' => [
            'account_id' => 1,
            'name' => 'Course Name',
            'term_id' => 6,
          ],
        ],
        'course[account_id]=1&course[name]=Course Name&course[term_id]=6'
      ]
    ];
  }

  /**
   * @dataProvider buildQueryProvider
   */
  public function testBuildQuery($params, $expected) {
    $string = $this->api->buildQuery($params);
    $this->assertEquals($expected,$string);
  }

  public function testGetUrlString() {
    $url = $this->api->getUrlString();
    $expected = 'https://test_school.test.instructure.com/api/v1/';
    $this->assertEquals($expected, $url);
  }
}
