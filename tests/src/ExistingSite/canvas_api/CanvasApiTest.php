<?php
namespace Drupal\Tests\canvas_api\ExistingSite;

use weitzman\DrupalTestTraits\ExistingSiteBase;

class CanvasApiTest extends ExistingSiteBase {

  private $service;

  protected function setUp(): void {
    parent::setUp();
    $this->service = \Drupal::service('canvas_api');
  }

  public function testConfig() {
    $apiSettings = \Drupal::config('canvas_api.settings');
    $lmsSettings = \Drupal::config('canvas_lms.settings');
    $this->assertNotEmpty($lmsSettings->get('institution'),'No institution set');
    $this->assertNotEmpty($lmsSettings->get('environment'),'No environment set');
    $this->assertNotEmpty($apiSettings->get('key'),'No key set');
  }

  public function testAccountsApi() {
    $results = $this->service
      ->setMethod('GET')
      ->setPath('accounts')
      ->request();
    $this->assertGreaterThan(0, count($results));
    $this->assertArrayHasKey('id',reset($results));
  }

}


